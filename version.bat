@echo on
@echo =============================================================
@echo $                                                           $
@echo $                     Uncode SpringCloud                    $
@echo $                                                           $
@echo $                                                           $
@echo $                                                           $
@echo $  Uncode Studio All Right Reserved                         $
@echo $  Copyright (C) 2017-2050                                  $
@echo $  Author Yeweijun                                          $
@echo $                                                           $
@echo =============================================================
@echo.
@echo off

@title Uncode SpringCloud
@color 0a

call mvn versions:set -DnewVersion=0.0.5-SNAPSHOT

pause