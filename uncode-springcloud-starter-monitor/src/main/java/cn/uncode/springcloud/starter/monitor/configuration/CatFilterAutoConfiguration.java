package cn.uncode.springcloud.starter.monitor.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dianping.cat.servlet.CatFilter;

import cn.uncode.springcloud.starter.monitor.cat.CatMybatisInterceptor;
import cn.uncode.springcloud.starter.monitor.cat.CatPageURIRewriteAspect;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author juny
 * @date 2019年1月29日
 *
 */
@Slf4j
@Configuration
@ConditionalOnProperty(value = "info.cat.enabled", havingValue = "true")
public class CatFilterAutoConfiguration {

    @Bean
    public FilterRegistrationBean<CatFilter> catFilter() {
        FilterRegistrationBean<CatFilter> registration = new FilterRegistrationBean<CatFilter>();
        CatFilter filter = new CatFilter();
        registration.setFilter(filter);
        registration.addUrlPatterns("/*");
        registration.setName("cat-filter");
        registration.setOrder(1);
        log.info("===Uncode-starter===CatFilterAutoConfiguration===>CatFilter inited...");
        return registration;
    }
    
    @Bean
    public CatPageURIRewriteAspect catPageURIRewriteAspect() {
    	log.info("===Uncode-starter===CatFilterAutoConfiguration===>CatPageURIRewriteAspect inited...");
    	return new CatPageURIRewriteAspect();
    }
    
    @Bean
    public CatMybatisInterceptor catMybatisPlugin() {
    	log.info("===Uncode-starter===CatFilterAutoConfiguration===>CatMybatisInterceptor inited...");
    	return new CatMybatisInterceptor();
    }
}
