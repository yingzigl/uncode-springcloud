package cn.uncode.springcloud.admin.model.system.bo;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import lombok.Data;

/**
 * 过滤器定义模型
 */
@Data
public class FilterDefinition implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = -2335615817755196052L;

	/**
     * Filter Name
     */
    private String name;

    /**
     * 对应的路由规则
     */
    private Map<String, String> args = new LinkedHashMap<>();

}
