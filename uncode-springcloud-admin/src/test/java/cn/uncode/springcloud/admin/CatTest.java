package cn.uncode.springcloud.admin;

import com.dianping.cat.Cat;
import com.dianping.cat.message.Message;
import com.dianping.cat.message.Transaction;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CatTest {
    @Before
    public void initCat() {
        System.setProperty("app.name", "uncode-cat-test");
//        Cat.initializeByDomainForce("cat");
    }

    @Test
    public void transaction() {
        Transaction transaction = Cat.newTransaction("test", "test");
        transaction.setStatus(Message.SUCCESS);
        transaction.complete();
    }

    @Test
    public void event() {
        Cat.logEvent("test", "test");
    }

    @Test
    public void metric() {
        Cat.logMetricForCount("myKey", 1);
    }

    @After
    public void waitForSend() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ignored) {
        }
    }
    
    @Test
    public void testasList() {
            int[] datas = new int[]{1, 2, 3, 4, 5};
            List list = Arrays.asList(datas);

            // 输出：1
            System.out.println(list.size());

            list.add(6);

            // 输出：上一步抛出 Exception in thread "main" java.lang.UnsupportedOperationException
            System.out.println(list.size());
    }

}