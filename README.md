## Uncode-SpringCloud
Uncode-SpringCloud是一个基于最新的SpringCloud2.0的微服务开发脚手架，用于快速构建中大型系统的基础框架。将开发中遇到的问题和生产中所碰到的各种坑整理归纳，形成相应的解决方案融合到框架中。

# 功能概述

- 配置中心：Apollo。
- 服务注册与发现：Eureka，支持开发可以对节点状态进行操作。计划支持Nacos。
- 服务网关：Spring Gateway，支持动态路由和灰度。
- 断路保护和流量控制：Sentinel。
- 服务监控：CAT、SpringCloud Admin。
- 服务安全：Uncode Session。
- 消息通知：RibbitMQ、Event。
- 分布式事务：计划集成seata/fescar。
- 日志：ELK。
- 依赖组件：Uncode-DAL、Uncode-Cache、Uncode-Schedule、Uncode-Session。

## 架构图

![输入图片说明](https://images.gitee.com/uploads/images/2019/0719/102332_5f2f16db_277761.jpeg "SpringCloud架构 (1).jpg")

## 技术文档
* 即将发布，敬请期待，请start项目，给作者一些写文档的支持。

## 核心依赖

| 依赖                 | 版本          |
| -------------------- | ------------- |
| Spring Boot          | 2.0.x.RELEASE |
| Spring Cloud         | Finchley      |
| Spring Cloud Alibaba | 0.2.x.RELEASE |
| Uncode-DAL           | 2.2.5         |
| Uncode-Cache         | 2.0.5         |
| Uncode-Session       | 2.1.0         |
| Uncode-Schedule      | 1.1.0         |

## 工程结构

``` 
uncode-springcloud
├── uncode-springcloud-dependencies -- 依赖定义
├── uncode-springcloud-eureka -- 注册中心
├── uncode-springcloud-gateway -- Spring Cloud 网关
├── uncode-springcloud-utils -- 工具类
├── uncode-springcloud-starter-boot -- 启动、配置加载相关封装
├── uncode-springcloud-starter-bus -- 消息、事件、通知相关封装
├── uncode-springcloud-starter-fuse -- 熔断、限流、降级及调用链相关封装
├── uncode-springcloud-starter-log -- 操作日志、ELK、系统日志相关封装
├── uncode-springcloud-starter-canary -- 灰度发布相关封装
├── uncode-springcloud-starter-monitor -- 监控相关封装
├── uncode-springcloud-starter-web -- web相关功能封装
├── uncode-springcloud-starter-security -- 认证和受权相关功能封装
├── uncode-springcloud-parent -- 子应用需要继承的父pom
├── uncode-springcloud-admin -- 管理后台&demo
├── uncode-springcloud-demo -- demo
├    ├── uncode-springcloud-provider-api -- 服务提供api 
├    ├── uncode-springcloud-provider-impl -- 服务提供实现
└──  └── uncode-springcloud-consumer -- 服务消费demo
```


# 开源协议

Apache Licence 2.0 （[英文原文](http://www.apache.org/licenses/LICENSE-2.0.html)）

Apache Licence是著名的非盈利开源组织Apache采用的协议。该协议和BSD类似，同样鼓励代码共享和尊重原作者的著作权，同样允许代码修改，再发布（作为开源或商业软件）。

需要满足的条件如下：

* 需要给代码的用户一份Apache Licence

* 如果你修改了代码，需要在被修改的文件中说明。

* 在延伸的代码中（修改和有源代码衍生的代码中）需要带有原来代码中的协议，商标，专利声明和其他原来作者规定需要包含的说明。

* 如果再发布的产品中包含一个Notice文件，则在Notice文件中需要带有Apache Licence。你可以在Notice中增加自己的许可，但不可以表现为对Apache Licence构成更改。

Apache Licence也是对商业应用友好的许可。使用者也可以在需要的时候修改代码来满足需要并作为开源或商业产品发布/销售。

#  关于

作者：冶卫军（[ywj_316@qq.com](mailto:ywj_316@qq.com),微信:yeweijun）

技术支持QQ群：47306892


# 界面一览

![输入图片说明](https://images.gitee.com/uploads/images/2019/0719/102111_9fcf8a8a_277761.png "微信图片_20190718161017.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0719/102130_2afd7233_277761.png "微信图片_20190718161136.png")