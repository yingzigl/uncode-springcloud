package cn.uncode.springcloud.utils.obj;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.ReflectionUtils;

public class BOUitl{
	
	
	/**
	 * 数据存储实例转业务实例[dto->bo]
	 * @param dto 数据实例
	 * @param clazz 业务实例Class
	 * @return 业务实体实例
	 */
	public static <T> T dto2bo(Object dto, Class<?> clazz) {
		T bean = BeanUtil.newInstance(clazz);
		Method[] methods = clazz.getDeclaredMethods();
		Method vfdMethod = null;
		if(methods != null) {
			for(Method md:methods) {
				if("valueFromDTO".equals(md.getName())) {
					vfdMethod = md;
					break;
				}
			}
		}
		if(vfdMethod != null) {
			ReflectionUtils.invokeMethod(vfdMethod, bean, new Object[] {dto});
		}
		return bean;
	}
	
	/**
	 * 业务实体转数据存存储实例[bo->dto]
	 * @param bo 业务实例
	 * @return 数据存储实例
	 */
	public static <T> T bo2dto(BaseBO<T> bo) {
		return bo.toDTO();
	}
	
	/**
	 * 数据存储实例集合转业务实例集合[dto->bo]
	 * @param dtoList 数据实例集合
	 * @param clazz 业务实例Class
	 * @return 业务实体实例集合
	 */
	public static <T> List<T> dto2boList(List<?> dtoList, Class<?> clazz) {
		List<T> list = new ArrayList<>();
		if(dtoList != null) {
			for(Object dto : dtoList) {
				list.add(dto2bo(dto, clazz));
			}
		}
		return list;
	}
	
	/**
	 * 业务实例集合转数据存储实例集合[bo->dto]
	 * @param bo 业务实例集合
	 * @return 数据存储实例集合
	 */
	public static <T> List<T> bo2dtoList(List<BaseBO<T>> boList) {
		List<T> list = new ArrayList<>();
		if(boList != null) {
			for(BaseBO<T> bo : boList) {
				list.add(bo2dto(bo));
			}
		}
		return list;
	}
	

	
	
		

}
