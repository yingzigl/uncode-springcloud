package cn.uncode.springcloud.starter.canary.properties;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import cn.uncode.springcloud.starter.canary.api.StrategyModel;
import lombok.Data;


/**
 * 
 *info:
 *	canary:
 *		enabled: true
 *		releaseIps: 127.0.0.1,...,
 *		rcIps: 127.0.0.1,...,
 *		betaIps: 127.0.0.1,...,
 *		alphaIps: 127.0.0.1,...,
 * 
 * @author juny
 * @date 2019年1月18日
 *
 */
@Data
@ConfigurationProperties(prefix = "info.canary", ignoreInvalidFields = true)
public class CanaryProperties {
	
	private boolean enabled;
/*	*//**
	 * 灰度标识
	 *//*
	private List<String> flag;
	*//**
	 * 匹配类型
	 *//*
	private List<String> type;
	*//**
	 * 匹配字段名称
	 *//*
	private List<String> key;
	*//**
	 * 匹配字段值列表
	 *//*
	private List<String> value;*/
	
	private List<StrategyModel> strategy;

}
